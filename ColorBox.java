package ru.maa.box;

public class ColorBox extends Box{
    private Color color;

    public ColorBox(String name, int width, int height, int depth, Color color) {
        super(name, width, height, depth);
        this.color = color;
    }
}
