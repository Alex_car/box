package ru.maa.box;

public enum Color {
    WHITE,
    YELLOW,
    RED,
    BLUE,
    BLACK,
    GREEN
}
